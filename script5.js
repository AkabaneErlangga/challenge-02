function getSplitName(personName) {

    if (typeof (personName) != "string") {
        return "Error: Invalid type of parameter. Expected String";
    }
    const name = personName.split(' ');
    var nameObj = {
        "firstName": null,
        "middleName": null,
        "lastName": null,
    }

    switch (name.length) {
        case 1:
            nameObj.firstName = name[0];
            break;
        case 2:
            nameObj.firstName = name[0];
            nameObj.lastName = name[1];
            break;
        case 3:
            nameObj.firstName = name[0];
            nameObj.middleName = name[1];
            nameObj.lastName = name[2];
            break;
        default:
            return ("Error: This function only for 3 characters name")
    }
    return nameObj;
}


console.log(getSplitName("Aldi Daniela Pranata"));

console.log(getSplitName("Dwi Kuncoro"));

console.log(getSplitName("Aurora"));

console.log(getSplitName("Aurora Aureliya Sukma Darma"));

console.log(getSplitName(0));
