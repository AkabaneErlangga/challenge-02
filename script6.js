function getAngkaTerbesarKedua(params) {
    if (typeof(params) != "object") {
        return "Error: Invalid type of parameter. Expected an object."
    }
    let max = Math.max.apply(null, params);
    params[params.indexOf(max)] = -Infinity;
    return Math.max.apply(null, params);
}

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8]

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());