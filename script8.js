const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang - Pergi',
        penulis: 'Tere Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tere Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,
    },
];

function getInfoPenjualan(dataPenjualan) {
    let obj = {
        "totalKeuntungan": null,
        "totalModal": null,
        "produkBukuTerlaris": null,
        "persentaseKeuntungan": null,
        "penulisTerlaris": null
    }

    dataPenjualan.forEach(data => {
        obj.totalKeuntungan += (data.hargaJual - data.hargaBeli) * data.totalTerjual;
        obj.totalModal += (data.totalTerjual + data.sisaStok) * data.hargaBeli;
    });

    obj.produkBukuTerlaris = dataPenjualan.reduce((max, data) => max.totalTerjual > data.totalTerjual ? max : data).namaProduk;

    obj.penulisTerlaris = dataPenjualan.reduce((prevData, data) => {
        if (data.penulis == prevData.penulis) {
            prevData.terjual += data.totalTerjual;
        } else {
            if (data.totalTerjual > prevData.terjual) {
                prevData.penulis = data.penulis;
                prevData.terjual = data.totalTerjual;
            }
        }

        return prevData;
    }, {
        penulis: null,
        terjual: 0
    }).penulis;
    
    obj.persentaseKeuntungan = (obj.totalKeuntungan / obj.totalModal * 100).toString() + ' %';

    return obj;
}

console.log(getInfoPenjualan(dataPenjualanNovel));