function checkEmail(email) {
    const regex = /([a-z]+@[a-z]+\.)\w+/;
    
    if (typeof(email) != "string") {
        return "ERROR: Parameter must be String";
    }
    if (!email.match("@")) {
        return "ERROR: email must contain '@'";
    }
    if (regex.test(email)) {
        return "VALID";
    }
    return "INVALID";
}

console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata'));
console.log(checkEmail(3322));
console.log(checkEmail());