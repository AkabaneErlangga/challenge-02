function isValidPassword(password) {
    if (typeof(password) == "string") {

        if (password.match(/(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}/)) {
            return true;
        }
        return false;
    }
    return "ERROR: parameter must be String"
}

console.log(isValidPassword('Meong2021'));
console.log(isValidPassword('meong2021'));
console.log(isValidPassword('@eong'));
console.log(isValidPassword('Meong2'));
console.log(isValidPassword(0));
console.log(isValidPassword());